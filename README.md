### libraQuest ###

libraQuest will search the Swirbul card catalog for users' search terms, returning a title, description, and call number for all relevant media. Users will then be able to press 'Find Me!' to receive step-by-step directions to the stack, and, in a further version, the shelf, that a given title will appear on. Link is to the bitbucket, app is for Android!