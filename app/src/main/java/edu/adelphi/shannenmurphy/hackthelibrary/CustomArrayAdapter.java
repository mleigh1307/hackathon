package edu.adelphi.shannenmurphy.hackthelibrary;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Philip Cohn-Cort on 3/13/15.
 */
public class CustomArrayAdapter extends ArrayAdapter<Result> {
	private static final int idTitle = R.id.titletext;
	private static final int idImage = R.id.resultImage;
	private static final int idDescription = R.id.resultDescription;
	private static final int idCallNumber = R.id.resultCallNumber;
	private final LayoutInflater inflater;
	private final Context context;

	public CustomArrayAdapter(Context context) {
		super(context, R.layout.item);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View container = convertView;
		if (container == null) {
			container = inflater.inflate(R.layout.item, parent, false);
		}
		final Result result = getItem(position);

		TextView title = (TextView) container.findViewById(idTitle);
		TextView description = (TextView) container.findViewById(idDescription);
		TextView callNumber = (TextView) container.findViewById(idCallNumber);
		ImageView image = (ImageView) container.findViewById(idImage);

		title.setText(result.title);
		description.setText(result.description);
		callNumber.setText(result.getCallNumber());
		image.setImageDrawable(context.getResources().getDrawable(result.imageId));
		container.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent launchIntent = new Intent(context, ResultActivity.class);
				launchIntent.putExtra("thing", result);
				context.startActivity(launchIntent);
			}
		});
		return container;
	}

	public void setBackingList(ArrayList<Result> results) {
		clear();
		addAll(results);
	}
}
