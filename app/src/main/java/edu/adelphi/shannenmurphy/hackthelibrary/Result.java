package edu.adelphi.shannenmurphy.hackthelibrary;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

/**
 * Created by Philip Cohn-Cort on 3/13/15.
 */
public class Result implements Parcelable {
	private static final String TAG = Result.class.getSimpleName();
	public String title;
	public String description;
	public CallNumber callNumber;
	public boolean currentlyOnShelf;
	public int imageId;

	public Result(String title, String description, String callNumber, boolean currentlyOnShelf) {
		this.title = title;
		this.description = description;
		this.callNumber = new CallNumber(callNumber);
		this.currentlyOnShelf = currentlyOnShelf;
	}

	public Result(byte[] rawContent) {
		Log.w(TAG, new String(rawContent));
	}

	public Result(Parcel parcel) {
		title = parcel.readString();
		description = parcel.readString();
		callNumber = new CallNumber(parcel.readString());
		imageId = parcel.readInt();
	}

	public String getCallNumber() {
		return callNumber.callCode;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(title);
		dest.writeString(description);
		dest.writeString(callNumber.callCode);
		dest.writeInt(imageId);
	}

	private class CallNumber {
		final String callCode;

		public CallNumber(String code) {
			callCode = code;
		}
	}

	public static final Parcelable.Creator<Result> CREATOR
			= new Parcelable.Creator<Result>() {
		public Result createFromParcel(Parcel in) {
			return new Result(in);
		}

		public Result[] newArray(int size) {
			return new Result[size];
		}
	};
}
