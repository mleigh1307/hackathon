package edu.adelphi.shannenmurphy.hackthelibrary;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import org.yaz4j.*;
import org.yaz4j.exception.ZoomException;

import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Iterator;


public class LandingScreenActivity extends ActionBarActivity {

	private ListView listView;
	private CustomArrayAdapter adapter;
	private Handler queryHandler = new Handler();

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing_screen);
        final EditText searchBar = (EditText) findViewById(R.id.searchbar);
        final ImageButton searchButton = (ImageButton) findViewById(R.id.searchbutton);
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String text = searchBar.getText().toString();
                searchWithText(text);
            }
        });
		listView = (ListView) findViewById(R.id.entries);
		adapter = new CustomArrayAdapter(this);
		listView.setAdapter(adapter);
    }

    private void searchWithText(String text) {
		findCallCodes(text);
    }

	private void findCallCodes(String text) {
		queryHandler.post(new SimpleQuery(text));
	}

	private void applyResults(@NonNull ArrayList<Result> results) {
		adapter.setBackingList(results);
	}


	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_landing_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

	private class SimpleQuery implements Runnable {
		private final Query query;

		public SimpleQuery(String text) {
			query = null;
			//query = new CQLQuery(text);
		}

		@Override
		public void run() {
			ArrayList<Result> results = new ArrayList<>();
			Result dummy = new Result("Astonishing X-Men.", "Dream-team creators Joss Whedon (TV's Buffy the Vampire Slayer) and John " +
					"Cassaday (Planetary, Captain America) present the explosive, all-new flagship X-Men series, marking a return to " +
					"classic greatness and the beginning of a brand-new era for the X-Men! Cyclops and Emma Frost re-form the X-Men with the express purpose of \"astonishing\" the world. But when breaking news regarding the mutant gene unexpectedly hits the airwaves, will it derail their new plans before they even get started? As demand for the \"mutant cure\" reaches near-riot levels, the X-Men go head-to-head with the enigmatic Ord, with an unexpected ally, and some unexpected adversaries, tipping the scales!",
					"PN6728.A87 W484 2004", false);
			dummy.imageId = R.drawable.sample;
			Result alt = new Result("Inheritance : how our genes change our lives, and our lives change our genes", " \tIn this book, the author, a physician and writer employs his wide-ranging and interdisciplinary approach to science and medicine, explaining how art, history, superheroes, sex workers, and sports stars all help us understand the impact of our lives on our genes, and our genes on our lives. He explains new concepts in human genetics and health that indicate that the fundamental nature of the human genome is much more fluid and flexible than originally thought. He reveals how genetic breakthroughs are completely transforming our understanding of both the world and our lives. Conventional wisdom dictates that our genetic destiny is fixed at conception. But this book shows us that the human genome is far more fluid and fascinating than your ninth grade biology teacher ever imagined. By bringing us to the bedside of his unique and complex patients, he demonstrates what rare genetic conditions can teach us all about our own health and well-being. In the brave new world we are rapidly rocketing into, genetic knowledge has become absolutely crucial. This book provides a roadmap for this journey by teaching: Why you may have recovered from the psychological trauma caused by childhood bullying, but your genes may remain scarred for life ; How fructose is the sugar that makes fruits sweet, but if you have certain genes, consuming it can buy you a one-way trip to the coroner's office ; Why ingesting common painkillers is like dosing yourself repeatedly with morphine, if you have a certain set of genes ; How insurance companies legally use your genetic data to predict the risk of disability for you and your children, and how that impacts the coverage decisions they make for your family ; How to have the single most important conversation with your doctor, one that can save your life ; And finally, Why people with rare genetic conditions hold the keys to medical problems affecting millions. This book will alter how you view your genes, your health, and your life",
					"RB155 .M58 2014", false);
			alt.imageId = R.drawable.alt;
			results.add(dummy);
			results.add(alt);

			/*
			try {
				Connection connection = new Connection("alicatclassic.adelphi.edu", 80);
				connection.connect();
				ResultSet records = connection.search(query);
				Iterator<Record> recordIterator = records.iterator();
				Record record;
				while (recordIterator.hasNext()) {
					record = recordIterator.next();
					byte[] rawContent = record.getContent();// Raw content is an array of bytes.
					Result result = new Result(rawContent);
					results.add(result);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}*/
			applyResults(results);
		}
	}
}
